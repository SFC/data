### Launch all internet2 instances
```bash
parallel --bar --linebuffer -j1 ../build/Partial/OccLim/LP_Partial_OCC ::: $(find instances/partial/ -name "internet2_*.inst") ::: -r ::: {10..-1..1} ::: -m ::: path_rounding ::: -v ::: -j ::: 3 ::: -a  ::: .1
```

### Plots
```bash
python2.7 script/partial/occlim/plot.py internet2_1000000_5_15_1.000000 -m path_roundin
```

