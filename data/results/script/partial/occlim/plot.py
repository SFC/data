import json
import matplotlib
import matplotlib.pyplot as plt
import argparse
import math
import os 

algos = {
	"path": 0,
	"path_rounding": 0,
	"bab_master": 0,
	"bab_master_stab": 0
}

def getOrder(name):
	if name.startswith("internet2"):
		return 10
def getBandwidthLimits(name):
	if name.startswith("internet2"):
		return [1800, 2900]

def latexify(fig_width=None, fig_height=None, columns=1, fontsize=25):
	'''
	Set parameters for matplotlib style to obtain IEEE figures
	'''
	assert columns in [1, 2]
	fig_widths = [3.39, 6.9]
	if fig_width is None:
	   fig_width = fig_widths[columns-1] # width in inches9

	if fig_height is None:
	   fig_height = fig_width*((math.sqrt(5)-1.0)/2.0) # height in inches

	MAX_HEIGHT_INCHES = 8.0
	if fig_height > MAX_HEIGHT_INCHES:
		print("WARNING: fig_height too large:" + fig_height +
        "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
		fig_height = MAX_HEIGHT_INCHES

	params = {'backend': 'pdf',
           # 'text.latex.preamble': ['\usepackage{gensymb}'],
           'axes.labelsize': 10, # fontsize for x and y LABELS (was 10)
           'axes.titlesize': 10,
           'font.size': fontsize, # was 10
           'legend.fontsize': 10, # was 10
           'xtick.labelsize': 10,
           'ytick.labelsize': 10,
           # 'text.usetex': True,
           'figure.figsize': [fig_width, fig_height],
           'font.family': 'serif',
	}
	matplotlib.rcParams.update(params)

def plot_bandwidth_vs_nbLicenses(instanceName, model="path", seed_range=[0]):
	x = []
	bandwidth = []
	lowerBound = []
	for nbLicenses in xrange(1, getOrder(instanceName)+1):
		for seed in seed_range:
			filename = "./results/partial/occlim/{}_{}_{}_{}.res".format(instanceName, seed, nbLicenses, model)
			try:
				with open(filename) as data_file:
					data = json.load(data_file)
					bandwidth.append(data["objValue"])
					lowerBound.append(data["lowerBound"])
					x.append(nbLicenses)
			except:
				pass
	
	plt.clf()
	fig, ax = plt.subplots()
	ax.plot(x, bandwidth, label="Solution", marker="+")
	ax.plot(x, lowerBound, label="Lower bound")
	ax.set(xlabel='# licenses', ylabel='Bandwidth')
	ax.legend()
	if(not os.path.isdir("./figures/partial/occlim/")):
		os.makedirs("./figures/partial/occlim/")
	fig.savefig("figures/partial/occlim/band_nbLicenses_{}_{}.pdf".format(instanceName, model))
	fig.show()

def plot_bandwidthAndTime_vs_nbLicenses(instanceName, model="path", seed_range=[0]):
	x = []
	bandwidth = []
	time = []
	for nbLicenses in xrange(1, getOrder(instanceName)+1):
		for seed in seed_range:
			filename = "./results/partial/occlim/{}_{}_{}_{}.res".format(instanceName, seed, nbLicenses, model)
			try:
				with open(filename) as data_file:
					data = json.load(data_file)
					bandwidth.append(data["objValue"] / 10000.0)
					time.append(data["WallTime"])
					x.append(nbLicenses)
			except:
				pass
	
	plt.clf()
	fig, ax1 = plt.subplots()
	ax2 = ax1.twinx()

	ax1.bar(x, time, width=0.5)
	ax1.set(ylabel='Time', yscale="log")
	ax1.set_yticks([1, 1000, 60*1000, 60*60*1000])
	ax1.set_yticklabels(["1ms", "1s", "10min", "1h"])

	ax2.plot(x, bandwidth, label="Solution", marker="+", color="black")
	ax2.set(xlabel='# licenses', ylabel='Bandwidth', ylim=getBandwidthLimits(instanceName))

	fig.legend()
	if(not os.path.isdir("./figures/partial/occlim/")):
		os.makedirs("./figures/partial/occlim/")
	fig.tight_layout()
	fig.show()
	fig.savefig("figures/partial/occlim/bandTime_nbLicenses_{}_{}.pdf".format(instanceName, model))
	

def main():
	parser = argparse.ArgumentParser(description='Plots')
	parser.add_argument('instance', help='name of the instance')
	parser.add_argument('-m', dest="method", choices=algos.keys(), default="path", help='name of the algorithm to call')
	args = parser.parse_args()

	# plot_bandwidth_vs_nbLicenses(args.instance, args.method)
	plot_bandwidthAndTime_vs_nbLicenses(args.instance, args.method)

if __name__ == '__main__':
	main()