import json
import argparse
import subprocess
from pathlib import Path


class MatterMostObserver:
    url = 'https://mattermost.imt-atlantique.fr/hooks/7kaomphni3f9zcy7oyq54c8jqc'

    def register_experiments(self, experiments):
        self.experiments = experiments

    def run_all(self, run_function):
        for experiment in self.experiments:
            run_function(*experiment)


def run(method, input_file, output_file, nb_licence):
    command_line = f"/home/nhuin/src/SFC_CG/build/rl/src/OccLim/PlacementOccLim {method} {input_file} {nb_licence} -o {output_file} -j 48"
    print(f"Running {command_line}")
    with subprocess.Popen(command_line.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
        outs, errs = proc.communicate()
        print(errs)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dryrun", help="only print the experiement to run",
                        action="store_true")
    args = parser.parse_args()

    experiments = []
    for instance in ["atlanta", "germany50"]:
        input_file = Path(
            f"/home/nhuin/src/SFC_CG/data/instances/occlim/{instance}.json")
        with open(input_file) as fd:
            order = json.load(fd)["order"]
        for nb_licence in range(order, 0, -1):
            for method in ["CRGBnB", "CGBnB"]:
                output_file = Path(
                    f"/home/nhuin/src/SFC_CG/results/{instance}_{method}_{nb_licence}.json")
                if output_file.is_file():
                    continue
                experiments.append(
                    (method, input_file, output_file, nb_licence))

    if args.dryrun:
        print(experiments)
    else:
        observer = MatterMostObserver()
        observer.register_experiments(experiments)
        observer.run_all(run)
